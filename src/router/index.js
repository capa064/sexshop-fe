import { createRouter, createWebHashHistory } from 'vue-router'
import App from '../App.vue'

import LogIn from '../components/LogIn'
import SignUp from '../components/SignUp'
import Main from '../components/Main'
import Products from '../components/Products'

const routes = [
  {
    path: '/',
    name: 'App',
    component: App
  },
  {
    path: '/user/main',
    name: 'Main',
    component: Main
  },
  {
    path: '/user/logIn',
    name: 'logIn',
    component: LogIn
  },
  {
    path: '/user/signUp',
    name: 'signUp',
    component: SignUp
  },
  {
    path: '/user/products',
    name: 'products',
    component: Products
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
